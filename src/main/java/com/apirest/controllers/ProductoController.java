package com.apirest.controllers;

import com.apirest.models.ProductoModel;
import com.apirest.models.ProductoPrecioOnly;
import com.apirest.services.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("${url.base}")
public class ProductoController {

    @Autowired
    private ProductoService productService;

    @GetMapping("")
    public String root() {
        return "ProdLeo API REST v1";
    }

    // GET all products
    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productService.getProductos();
    }

    // GET size
    @GetMapping("/productos/size")
    public int countProductos() {
        return productService.getProductos().size();
    }

    // GET instancia por id
    @GetMapping("/productos/{id}")
    public ResponseEntity getProductoId(@PathVariable int id) {
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado!", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }

    // POST
    @PostMapping("/productos")
    public ResponseEntity<String> addProducto(@RequestBody ProductoModel productoModel) {
        productService.addProducto(productoModel);
        return new ResponseEntity<>("Producto correctamente creado!", HttpStatus.CREATED);
    }

    // PUT
    @PutMapping("/productos/{id}")
    public ResponseEntity updateProducto(@PathVariable int id,
                                         @RequestBody ProductoModel productToUpdate) {
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado!", HttpStatus.NOT_FOUND);
        }
        productService.updateProducto(id - 1, productToUpdate);
        return new ResponseEntity<>("Producto correctamente actualizado!", HttpStatus.OK);
    }

    // DELETE
    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProducto(@PathVariable Integer id) {
        ProductoModel pr = productService.getProducto(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado!", HttpStatus.NOT_FOUND);
        }
        productService.removeProducto(id - 1);
        return new ResponseEntity<>("Producto correctamente eliminado!", HttpStatus.OK); // También 204 No Content
    }

    // PATCH
    @PatchMapping("/productos/{id}")
    public ResponseEntity patchPrecioProducto(@RequestBody ProductoPrecioOnly productoPrecioOnly,
                                                 @PathVariable int id){
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        pr.setPrecio(productoPrecioOnly.getPrecio());
        productService.updateProducto(id-1, pr);
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }

    // GET subrecurso
    @GetMapping("/productos/{id}/users")
    public ResponseEntity getProductIdUsers(@PathVariable int id){
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
           return new ResponseEntity<>("Producto no encontrado!", HttpStatus.NOT_FOUND);
        }
        if (pr.getUsers()!=null)
            return ResponseEntity.ok(pr.getUsers());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}